SRC = *.tex */*.tex

all: cvAalokSathe.pdf cvAalokSathe.html #clean

cvAalokSathe.pdf: $(SRC)
	latexmk -xelatex cvAalokSathe

cvAalokSathe.html: $(SRC)
	sed 's/4pt%REPL0/0pt/g' cvAalokSathe.tex > cvAalokSathe-web.tex
	make4ht cvAalokSathe-web.tex -x
	mv cvAalokSathe-web.html cvAalokSathe.html
	mv cvAalokSathe-web.css public/
	cp cvAalokSathe.html public/index.html
	rm -f *.4ct *.4tc *.xref *.tmp *.idv *.lg


clean:
	latexmk -C
	rm -f *.xdv *.synctex.gz *.4ct *.4tc *.xref *.tmp *.idv *.lg #*.pdf
